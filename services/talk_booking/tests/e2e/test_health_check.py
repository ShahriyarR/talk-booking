import requests


def test_health_check():
    response = requests.get("https://development.deepart.az/health-check/")

    assert response.status_code == 200
    assert response.text == '{"message":"OK"}'
